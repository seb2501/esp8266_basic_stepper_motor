from machine import Pin, PWM
import time

pin1 = Pin(14, Pin.OUT)
pin2 = Pin(12, Pin.OUT)
pin3 = Pin(13, Pin.OUT)
pin4 = Pin(15, Pin.OUT)

delay_ms = 1

# Séquence 1
Seq1 = []
Seq1.append([1,1,0,0])
Seq1.append([0,1,1,0])
Seq1.append([0,0,1,1])
Seq1.append([1,0,0,1])    

# Séquence 2
Seq2 = []
Seq2.append([0,0,0,1])
Seq2.append([0,0,1,1])
Seq2.append([0,0,1,0])
Seq2.append([0,1,1,0])
Seq2.append([0,1,0,0])    
Seq2.append([1,1,0,0])
Seq2.append([1,0,0,0])
Seq2.append([1,0,0,1])

def setOutputs(p1, p2, p3, p4):
    pin1.value(p1)
    pin2.value(p2)
    pin3.value(p3)
    pin4.value(p4)

while True:
    
    for seq in Seq2:
        setOutputs(seq[0],seq[1],seq[2],seq[3])
        time.sleep_ms(3)
